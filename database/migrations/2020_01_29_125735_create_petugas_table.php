<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePetugasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('petugas', function(Blueprint$table){
            $table->increments('id_petugas');
            $table->char('nama_petugas');
            $table->enum('gender',['Laki-Laki','Perempuan']);
            $table->char('no_hp');
            $table->char('email');

        });

            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('petugas');
    }
}
