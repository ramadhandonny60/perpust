<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeminjamanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peminjaman', function(Blueprint$table){

            $table->increments('nomor_pinjam');
            $table->char('kode_buku');
            $table->char('id_user');//dari sesiion user ketika login di ambilnya nanti
            $table->date('tgl_pinjam'); //startdate
            $table->date('tgl_pengembalian'); //enddate
            $table->char('status_peminjaman'); // di kembalikan | belom di kembalikan
            $table->char('lama_pinjam'); //jumlahnya hari,lebih baik pake varchar string
    

        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peminjaman');
    }
}
