<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBukuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buku',function(Blueprint $table){
            $table->char('kode_buku');
            $table->char('judul_buku');
            $table->char('penulis');
            $table->char('penerbit');
            $table->char('tahun_rilis');
            $table->char('kategori');
            $table->text('jumlah');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buku');
    }
}
