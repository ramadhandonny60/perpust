<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuruTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guru',function(Blueprint $table){
            $table->increments('id_guru');
            $table->char('nama_guru');
            $table->enum('gender',['Laki-Laki','Perempuan']);
            $table->char('no_hp');
            $table->char('email');
            $table->text('alamat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
