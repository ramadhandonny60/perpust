<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswa',function(Blueprint $table){
            $table->increments('id_siswa');
            $table->char('nama_siswa');
            $table->char('kelas');
            $table->enum('gender',['Laki-Laki','Perempuan']);
            $table->char('no_hp');
            $table->char('email');
            $table->text('alamat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
